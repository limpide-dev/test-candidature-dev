# Test limpide dev

Un développeur nous a fourni ce formulaire, mais il contient beaucoup de problèmes.  
Voici ceux que nous avons identifiés et qu'il faut corriger :  
- L'utilisateur peut rajouter l'attribut "novalidate" sur le formulaire ou retirer les attributs "required" sur les champs pour outre-passer la validation.  
- L'utilisateur peut modifier le HTML via l'inspecteur pour mettre un sujet qui n'est pas dans la liste des choix initialement prévue. Il faut vérifier que le sujet sélectionné soit bien dans cette liste.  
- L'utilisateur peut copier/coller les caractères à recopier dans le champs "human-test". Faire en sorte qu'on ne puisse pas faire cela.  
- Nous aimerions également avertir l'utilisateur que le champs "human-test" est incorrecte avant la soumission du formulaire (via Javascript).

Il existe peut-être d'autres problèmes que nous n'avons pas identifiés.

De plus, ce développeur n'as appliqué aucun style au formulaire.  
Nous aimerions que les champs "prénom" et "nom" soient côte à côte.  

Nous vous laissons carte blanche pour le reste. Si vous souhaitez nous proposer un peu de style, nous ne sommes pas contre.

## Aide au développement

Pour lancer un serveur php, rendez-vous dans un terminal, naviguez jusqu'au dossier contenant les fichiers de l'exercice et tapez cette commande `php -S localhost:8080`.  
Vous pourrez ensuite accéder au site via l'url `http://localhost:8080`.

## Exercice selon type de candidat

> Si vous postulez en tant que développeur **back-end**, vous pouvez ne gérer que la partie PHP de cet exercice.  

> Si vous postulez en tant que développeur **front-end**, vous pouvez ne gérer que la partie CSS et JS de cet exercice.  

> Si vous décidez de faire **l'intégralité du test**, nous ne vous jugerons que sur votre domaine d'expertise. Le reste, ce n'est que du bonus.

## Point d'attention

Nous souhaitons voir **votre** code. Merci de ne pas utiliser des librairies qui feraient le travail à votre place (jQuery, Bootstrap, etc..).  

Vous êtes cependant libres d'utiliser des pré-processeurs, par exemple, pour écrire du SASS.  

Vous pouvez aussi, si vous le souhaitez, ré-écrire / améliorer le code, du moment que le résultat final corresponde à nos attentes (c'est à dire un formulaire fonctionnel, sans risque d'erreurs).  

## Rendu

Envoyez un mail à `hatouma.traore@limpide.fr` en nous précisant pour quel poste vous postulez (développeur front / back / les deux) et fournissez-nous le dossier contenant les fichiers du test.  
Pour ce faire, vous pouvez nous envoyer un zip via la plateforme de votre choix (mail, wetransfer, etc..), ou via un repository git publique.  
Évitez cependant de nous transmettre les dossiers `node_modules/` et `vendor/` si vous utilisez `npm / yarn` et `composer`.
