<?php

/**
 * Création d'une chaîne de caractère aléatoire
 * 
 * @param integer $stringLength Longueur de la chaîne de caractère
 * 
 * @return string
 */
function getRandomString(int $stringLength = 5) : string
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';

    for ($i = 0; $i < $stringLength; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomString .= $characters[$index];
    }

    return $randomString;
}

session_start();

$subjects = [
    'order_issue' => 'Problème sur ma commande',
    'order_incomplete' => 'Commande incomplète',
    'order_not_received' => 'Commande non reçue',
];
$humanTestChars = getRandomString(10);
$errors = [];
$success = false;

if (isset($_POST['firstname'])) { // Formulaire envoyé
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $humanTest = $_POST['human-test'];

    if ($humanTest !== $_SESSION['human-test-chars']) {
        $errors['human-test'] = 'Les caractères ne correspondent pas.';
    }

    if (empty($errors)) {
        $success = true;
    }
}

$_SESSION['human-test-chars'] = $humanTestChars;
?>
<!DOCTYPE html>
<html>
<head>
    <title>Test candidature Limpide</title>
    <link rel="stylesheet" href="./app.css">
    <script src="./app.js"></script>
</head>
<body>
    <?php if (!$success) : ?>
        <form method="POST">
            <div>
                <label for="firstname">Prénom*</label>
                <input type="test" name="firstname" id="firstname" value="<?php echo $firstname ?? ''; ?>" required>
            </div>

            <div>
                <label for="lastname">Nom*</label>
                <input type="text" name="lastname" value="<?php echo $lastname ?? ''; ?>" required>
            </div>

            <div>
                <label for="email">Email</label>
                <input type="text" id="email" value="<?php echo $email ?? ''; ?>">
            </div>

            <div>
                <label for="subject">Sujet*</label>
                <select name="subject" id="subject" required>
                    <option>Sélectionnez un sujet</option>
                    <?php foreach ($subjects as $value => $label) : ?>
                        <option value="<?php echo $value; ?>"><?php echo $label; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div>
                <label for="message">Message*</label>
                <textarea name="message" id="message"><?php echo $message ?? ''; ?></textarea>
            </div>

            <div>
                <label for="human-test">Recopiez ces caractères* <strong><?php echo $humanTestChars; ?></strong></label>
                <input type="text" name="human-test" id="human-test" data-chars="<?php echo $humanTestChars; ?>" required>
                <?php if (isset($errors['human-test'])) : ?>
                    <div><?php echo $errors['human-test']; ?></div>
                <?php endif; ?>
            </div>

            <button type="submit">Envoyer</button>
        </form>
    <?php endif; ?>

    <?php if ($success) : ?>
        <div>Message reçu !</div>
    <?php endif; ?>
</body>
</html>
